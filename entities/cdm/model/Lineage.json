{
  "classifierPath" : "meta::pure::metamodel::type::Class",
  "content" : {
    "_type" : "class",
    "name" : "Lineage",
    "package" : "cdm::model",
    "properties" : [ {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "tradeReference",
      "stereotypes" : [ {
        "profile" : "cdm::model::metadata",
        "value" : "reference"
      } ],
      "type" : "cdm::model::Trade"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "eventReference",
      "stereotypes" : [ {
        "profile" : "cdm::model::metadata",
        "value" : "reference"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The reference to the instantiation of an Event object, either through a globalKey or an xml-derived id/href mechanism. The definition associated to the Lineage class provides more details with respect to those referencing approaches, their expected usage and available implementation."
      } ],
      "type" : "cdm::model::WorkflowStep"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "cashflowReference",
      "stereotypes" : [ {
        "profile" : "cdm::model::metadata",
        "value" : "reference"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The reference to the instantiation of a Cashflow payout component object. An expected typical usage is to provide lineage for the payment of, say, the option premium or the swap initial fee. The definition associated to the Lineage class provides more details with respect to those referencing approaches, their expected usage and available implementation."
      } ],
      "type" : "cdm::model::Cashflow"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "creditDefaultPayoutReference",
      "stereotypes" : [ {
        "profile" : "cdm::model::metadata",
        "value" : "reference"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The reference to the instantiation of a CreditdefaultPayout component object. The definition associated to the Lineage class provides more details with respect to those referencing approaches, their expected usage and available implementation."
      } ],
      "type" : "cdm::model::CreditDefaultPayout"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "interestRatePayoutReference",
      "stereotypes" : [ {
        "profile" : "cdm::model::metadata",
        "value" : "reference"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The reference to the instantiation of a InterestRatePayout component object. An expected typical usage is to provide lineage for the payment of, say, an interest rate swap reset, with the ability to relate the gross cashflow amounts to the respective payout components. The definition associated to the Lineage class provides more details with respect to those referencing approaches, their expected usage and available implementation."
      } ],
      "type" : "cdm::model::InterestRatePayout"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "optionPayoutReference",
      "stereotypes" : [ {
        "profile" : "cdm::model::metadata",
        "value" : "reference"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The reference to the instantiation of a OptionPayout component object. The definition associated to the Lineage class provides more details with respect to those referencing approaches, their expected usage and available implementation."
      } ],
      "type" : "cdm::model::OptionPayout"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "equityPayoutReference",
      "stereotypes" : [ {
        "profile" : "cdm::model::metadata",
        "value" : "reference"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The reference to the instantiation of a EquityPayout object. An expected typical usage is to provide lineage for the payment of, say, an equity dividend. The definition associated to the Lineage class provides more details with respect to those referencing approaches, their expected usage and available implementation."
      } ],
      "type" : "cdm::model::EquityPayout"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "transferReference",
      "stereotypes" : [ {
        "profile" : "cdm::model::metadata",
        "value" : "reference"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The reference to the instantiation of a TransferPrimitive object."
      } ],
      "type" : "cdm::model::TransferPrimitive"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "legalAgreement",
      "stereotypes" : [ {
        "profile" : "cdm::model::metadata",
        "value" : "reference"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The reference to the instantiation of a Legal Agreement object. The definition associated to the Lineage class provides more details with respect to those referencing approaches, their expected usage and available implementation."
      } ],
      "type" : "cdm::model::LegalAgreement"
    }, {
      "multiplicity" : {
        "lowerBound" : 0
      },
      "name" : "portfolioStateReference",
      "stereotypes" : [ {
        "profile" : "cdm::model::metadata",
        "value" : "reference"
      } ],
      "taggedValues" : [ {
        "tag" : {
          "profile" : "meta::pure::profiles::doc",
          "value" : "doc"
        },
        "value" : "The refence to the previous state of a Portfolio, in a chain of Events leading up to a build of that Portfolio as the holding of Product(s) in specific Quantity(ies). As part of the PortfolioState object, a pointer to the previous PortfolioState is provided through a Lineage object, together with pointer(s) to the Event or set of Events leading up to the current (new) state."
      } ],
      "type" : "cdm::model::PortfolioState"
    } ],
    "taggedValues" : [ {
      "tag" : {
        "profile" : "meta::pure::profiles::doc",
        "value" : "doc"
      },
      "value" : "A class to provide lineage information across lifecycle events through a pointer or set of pointers into the event(s), contract(s) and, possibly, payout components that the event is dependent on or relates to. As an example, if an contractFormation event is corrected, the correction event will have a lineage into the initial event, which takes the form of a globalKey into that initial contract formation event. Two referencing mechanisms are provided as part of the CDM: either the globalKey, which corresponds to the hash value of the CDM class which is referred to, or a reference qualifier which is meant to provide support for the ingestion of xml documents with id/href mechanisms. The CDM recommends the use of the globalKey and provides a default implementation which is accessible in the generated code through org.isda.cdm.globalKey.GlobalKeyHashCalculator. If implementers want to use an alternative hashing mechanism, the API in which they need to plug it is com.rosetta.model.lib.HashFunction."
    } ]
  }
}